import java.util.HashMap;
import java.util.Map;

public class Number {
    public int findRepeated(int n, int[] nums) {
        // find repeated n time
        HashMap<Integer, Integer> numsRepeated = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (!numsRepeated.containsKey(nums[i])) {
                numsRepeated.put(nums[i], 1);
            } else {
                int existingValue = numsRepeated.get(nums[i]);
                numsRepeated.replace(nums[i], existingValue + 1);
            }
        }

        if (numsRepeated.size() != (n+1)) System.out.println("nums[] doesn't contain n+1 unique values");

        for (Map.Entry<Integer, Integer> number : numsRepeated.entrySet()) {
            if (number.getValue() == n) {
                return number.getKey();
            }
        }

        return 0;
    }
}
