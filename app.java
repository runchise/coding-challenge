
/*
 * Copyright (c) 2022.
 * Created by: Yudanto Anas
 */
public class app {
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);

        if (n < 2 || n > 5000) System.out.println("n value is out of range");

        int length = n * 2;
        int[] nums = new int[length];

        boolean isOutOfRange = false;
        for (int i = 1; i < args.length; i++) {
            int num = Integer.parseInt(args[i]);
            if (num < 0 || num > 104) {
                isOutOfRange = true;
                break;
            }
            nums[i - 1] = num;
        }

        if (!isOutOfRange) {
            Number number = new Number();
            System.out.println(number.findRepeated(n, nums));
        } else {
            System.out.println("nums[i] is out of range");
        }
    }
}
